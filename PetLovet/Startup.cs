﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PetLovet.Startup))]
namespace PetLovet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
