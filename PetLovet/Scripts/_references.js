/// <autosync enabled="true" />
/// <reference path="../build/js/custom.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="../vendors/animate.css/gulpfile.js" />
/// <reference path="../vendors/autosize/build.js" />
/// <reference path="../vendors/bootstrap/Gruntfile.js" />
/// <reference path="../vendors/bootstrap/package.js" />
/// <reference path="../vendors/bootstrap-daterangepicker/daterangepicker.js" />
/// <reference path="../vendors/bootstrap-daterangepicker/package.js" />
/// <reference path="../vendors/bootstrap-datetimepicker/Gruntfile.js" />
/// <reference path="../vendors/bootstrap-progressbar/bootstrap-progressbar.js" />
/// <reference path="../vendors/bootstrap-wysiwyg/gruntfile.js" />
/// <reference path="../vendors/bootstrap-wysiwyg/gulpfile.js" />
/// <reference path="../vendors/chart.js/gulpfile.js" />
/// <reference path="../vendors/chart.js/karma.conf.ci.js" />
/// <reference path="../vendors/chart.js/karma.conf.js" />
/// <reference path="../vendors/chart.js/karma.coverage.conf.js" />
/// <reference path="../vendors/datejs/GruntFile.js" />
/// <reference path="../vendors/datejs/index.js" />
/// <reference path="../vendors/devbridge-autocomplete/gruntfile.js" />
/// <reference path="../vendors/echarts/index.common.js" />
/// <reference path="../vendors/echarts/index.js" />
/// <reference path="../vendors/echarts/index.simple.js" />
/// <reference path="../vendors/echarts/webpack.config.js" />
/// <reference path="../vendors/eve/eve.js" />
/// <reference path="../vendors/flot.curvedlines/curvedLines.js" />
/// <reference path="../vendors/flot/excanvas.js" />
/// <reference path="../vendors/flot/jquery.colorhelpers.js" />
/// <reference path="../vendors/flot/jquery.flot.canvas.js" />
/// <reference path="../vendors/flot/jquery.flot.categories.js" />
/// <reference path="../vendors/flot/jquery.flot.crosshair.js" />
/// <reference path="../vendors/flot/jquery.flot.errorbars.js" />
/// <reference path="../vendors/flot/jquery.flot.fillbetween.js" />
/// <reference path="../vendors/flot/jquery.flot.image.js" />
/// <reference path="../vendors/flot/jquery.flot.js" />
/// <reference path="../vendors/flot/jquery.flot.navigate.js" />
/// <reference path="../vendors/flot/jquery.flot.pie.js" />
/// <reference path="../vendors/flot/jquery.flot.resize.js" />
/// <reference path="../vendors/flot/jquery.flot.selection.js" />
/// <reference path="../vendors/flot/jquery.flot.stack.js" />
/// <reference path="../vendors/flot/jquery.flot.symbol.js" />
/// <reference path="../vendors/flot/jquery.flot.threshold.js" />
/// <reference path="../vendors/flot/jquery.flot.time.js" />
/// <reference path="../vendors/flot/jquery.js" />
/// <reference path="../vendors/icheck/icheck.js" />
/// <reference path="../vendors/jquery.easy-pie-chart/Gruntfile.js" />
/// <reference path="../vendors/jquery.hotkeys/jquery.hotkeys.js" />
/// <reference path="../vendors/jquery.tagsinput/Gruntfile.js" />
/// <reference path="../vendors/jquery-knob/excanvas.js" />
/// <reference path="../vendors/jquery-mousewheel/jquery.mousewheel.js" />
/// <reference path="../vendors/jqvmap/gruntfile.js" />
/// <reference path="../vendors/jszip/Gruntfile.js" />
/// <reference path="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" />
/// <reference path="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js" />
/// <reference path="../vendors/mjolnic-bootstrap-colorpicker/serve.js" />
/// <reference path="../vendors/mocha/mocha.js" />
/// <reference path="../vendors/moment/moment.js" />
/// <reference path="../vendors/morris.js/Gruntfile.js" />
/// <reference path="../vendors/morris.js/morris.js" />
/// <reference path="../vendors/nprogress/nprogress.js" />
/// <reference path="../vendors/parsleyjs/gulpfile.babel.js" />
/// <reference path="../vendors/raphael/raphael.js" />
/// <reference path="../vendors/raphael/raphael.no-deps.js" />
/// <reference path="../vendors/raphael/webpack.config.js" />
/// <reference path="../vendors/requirejs/require.js" />
/// <reference path="../vendors/select2/Gruntfile.js" />
/// <reference path="../vendors/skycons/skycons.js" />
/// <reference path="../vendors/switchery/package.js" />
/// <reference path="../vendors/switchery/switchery.js" />
/// <reference path="../vendors/transitionize/transitionize.js" />
/// <reference path="../vendors/validator/multifield.js" />
/// <reference path="../vendors/validator/validator.js" />
